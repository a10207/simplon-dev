<?php

// Verification qu'il y a exactement 1 argument
if ($argc != 2) {
    echo 'Incorrect Parameters' . "\n";
    exit();
}

function calcul($var1, $var2, $operation)
{
    // Opération mathématique en fonction de l'opérateur $operation
    switch ($operation) {
        case '+':
            echo $var1 + $var2 . "\n";
            break;

        case '-':
            echo $var1 - $var2 . "\n";
            break;

        case '*':
            echo $var1 * $var2 . "\n";
            break;

        case '/':
            if ($var2 == 0) {
                echo '0' . "\n";
                break;
            } else {
                echo $var1 / $var2 . "\n";
                break;
            }

            // no break
        case '%':
            echo $var1 % $var2 . "\n";
            break;

        default:
            echo 'Syntax Error' . "\n";
            break;
    }
}

$regex = "/^\s*(?<num1>[\+-]?\d*\.?\d+]?)\s*(?<ope>\+|\*|\/|\-|\%)\s*(?<num2>(?1))\s*$/";
if (!preg_match_all($regex, $argv[1], $array1, PREG_SET_ORDER)) {
    echo 'Syntax Error' . "\n";
    exit();
}

calcul($array1[0]['num1'], $array1[0]['num2'], $array1[0]['ope']);
