<?php

// je vérifie le nombre de paramètres
if ($argc > 2) {
    // je créé un tableau vide
    $tab = [];
    // j'assigne une valeur au premier élément
    $word = $argv[1];
    // je modifie mon tableau pour resortir la chaine avec key:value
    $array = array_splice($argv, 2);

    foreach ($array as $value) {
        // j'éclate la chaine pour ressortir une expression rationnelle
        $newValue = preg_split('/\W|s+/', $value, -1, PREG_SPLIT_NO_EMPTY);
        // je remplis mon tableau vide avec les nouvelles valeurs
        @$tab[$newValue[0]] = $newValue[1];
    }

    // je vérifie que ma variable a bien deux index
    if (count($newValue, 0) == 2) {
        // j'affiche ma valeur associé
        echo "$tab[$word]\n";
    }
}
