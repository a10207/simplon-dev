<?php

function ft_is_sort($tab)
{
    // je crée une variable temporaire "$temp" pour le différencier et pour vérifier que mon tableau initial "$tab"est trié ou non
    $temp = $tab;
    // je trie mon tableau temporaire
    sort($temp);
    // je vérifie si mon tableau temporaire trié est identique à mon tableau initial
    if ($temp == $tab) {
        return true;
    } else {
        return false;
    }
}
// return ($temp == $tab);
