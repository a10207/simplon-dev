<?php

if (!isset($argv[1])) {
    return;
}

$mot = $argv[1];

if ($argc == 2 && !empty($mot)) {
    // Rechercher et remplacer par expression rationnelle standard
    $mot = preg_replace('/\s+/', ' ', $mot);
    // trim = supprimer les espaces
    $mot = trim($mot);
    echo $mot;
    echo "\n";
}
