<?php

function ft_split($input)
{
    $tableau = preg_split("/[\s,]+/", $input, -1, PREG_SPLIT_NO_EMPTY);

    sort($tableau);

    return $tableau;
}
